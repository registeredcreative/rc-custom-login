<?php
/*
Plugin Name: rC Custom Login
Description: Easily rebranded custom WordPress login using SASS.
Version: 0.1
Author: Mark Branly, Registered Creative
Author URI: http://www.registeredcreative.com
*/
function rc_custom_login_head() {
    $css = plugin_dir_path(__FILE__) . 'css/login.css';

    if (!is_file($css)) {
        return;
    }

    $css = file_get_contents($css);
    // Convert relative URLs to absolute
    $css = str_replace(
        'url(../',
        sprintf('url(%s/', plugins_url('',__FILE__)),
        $css
    );
    $output = sprintf('<style type="text/css">%s</style>', $css);
    echo $output;
}
add_action('login_head','rc_custom_login_head', 99);

function rc_custom_login_headurl($url) {
    return home_url();
}
add_filter( 'login_headerurl', 'rc_custom_login_headurl' );