## Making Sure Sass Is Installed

Before you get started updating the style sheet,
make sure that you have SASS installed.

```
$ which sass
```

If you don't have it, you can install the Ruby GEM by following
the instructions on the [Sass project website](http://sass-lang.com/install).

If you're on a Mac later than 10.11, you'll need to use this:

```
$ sudo gem install -n /usr/local/bin sass
```

## Running Sass

When you are ready to start editing the stylesheet,
fire up a command line and go to the css folder
inside the project. Run this command to watch ``login.css``
for changes.

```
sass --watch -scss -t compressed --sourcemap=none login.scss
```

You can, of course, modify that to your liking. This command
will minify the output with no source map.

## Updating the CSS

Now, just open up ``css/login.scss`` in your favorite text
editor and change the variables to suit your needs. Of course,
you can change whatever you like throughout the sheet. But, in
most cases, configuring the variables should be all you need.


